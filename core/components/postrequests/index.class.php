<?php

/**
 * Class PostRequestsMainController
 */
abstract class PostRequestsMainController extends modExtraManagerController {
	/** @var PostRequests $PostRequests */
	public $PostRequests;


	/**
	 * @return void
	 */
	public function initialize() {
		$corePath = $this->modx->getOption('postrequests_core_path', null, $this->modx->getOption('core_path') . 'components/postrequests/');
		require_once $corePath . 'model/postrequests/postrequests.class.php';

		$this->PostRequests = new PostRequests($this->modx);
		$this->addCss($this->PostRequests->config['cssUrl'] . 'mgr/main.css');
		$this->addJavascript($this->PostRequests->config['jsUrl'] . 'mgr/postrequests.js');
		$this->addHtml('
		<script type="text/javascript">
			PostRequests.config = ' . $this->modx->toJSON($this->PostRequests->config) . ';
			PostRequests.config.connector_url = "' . $this->PostRequests->config['connectorUrl'] . '";
		</script>
		');

		parent::initialize();
	}


	/**
	 * @return array
	 */
	public function getLanguageTopics() {
		return array('postrequests:default');
	}


	/**
	 * @return bool
	 */
	public function checkPermissions() {
		return true;
	}
}


/**
 * Class IndexManagerController
 */
class IndexManagerController extends PostRequestsMainController {

	/**
	 * @return string
	 */
	public static function getDefaultController() {
		return 'home';
	}
}