<?php

/**
 * The home manager controller for PostRequests.
 *
 */
class PostRequestsHomeManagerController extends PostRequestsMainController {
	/* @var PostRequests $PostRequests */
	public $PostRequests;


	/**
	 * @param array $scriptProperties
	 */
	public function process(array $scriptProperties = array()) {
	}


	/**
	 * @return null|string
	 */
	public function getPageTitle() {
		return $this->modx->lexicon('postrequests');
	}


	/**
	 * @return void
	 */
	public function loadCustomCssJs() {
		$this->addCss($this->PostRequests->config['cssUrl'] . 'mgr/main.css');
		$this->addCss($this->PostRequests->config['cssUrl'] . 'mgr/bootstrap.buttons.css');
		$this->addJavascript($this->PostRequests->config['jsUrl'] . 'mgr/misc/utils.js');
		$this->addJavascript($this->PostRequests->config['jsUrl'] . 'mgr/widgets/items.grid.js');
		$this->addJavascript($this->PostRequests->config['jsUrl'] . 'mgr/widgets/items.windows.js');
		$this->addJavascript($this->PostRequests->config['jsUrl'] . 'mgr/widgets/home.panel.js');
		$this->addJavascript($this->PostRequests->config['jsUrl'] . 'mgr/sections/home.js');
		$this->addHtml('<script type="text/javascript">
		Ext.onReady(function() {
			MODx.load({ xtype: "postrequests-page-home"});
		});
		</script>');
	}


	/**
	 * @return string
	 */
	public function getTemplateFile() {
		return $this->PostRequests->config['templatesPath'] . 'home.tpl';
	}
}