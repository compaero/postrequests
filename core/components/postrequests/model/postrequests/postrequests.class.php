<?php

/**
 * The base class for PostRequests.
 */
class PostRequests {
	/* @var modX $modx */
	public $modx;
	public $request = array();
	public $requestAction = NULL;
	public $requestUrl = NULL;


	/**
	 * @param modX $modx
	 * @param array $config
	 */
	function __construct(modX &$modx, array $config = array()) {
		$this->modx =& $modx;

		$corePath = $this->modx->getOption('postrequests_core_path', $config, $this->modx->getOption('core_path') . 'components/postrequests/');
		$assetsUrl = $this->modx->getOption('postrequests_assets_url', $config, $this->modx->getOption('assets_url') . 'components/postrequests/');
		$connectorUrl = $assetsUrl . 'connector.php';

		$this->config = array_merge(array(
			'assetsUrl' => $assetsUrl,
			'cssUrl' => $assetsUrl . 'css/',
			'jsUrl' => $assetsUrl . 'js/',
			'imagesUrl' => $assetsUrl . 'images/',
			'connectorUrl' => $connectorUrl,

			'corePath' => $corePath,
			'modelPath' => $corePath . 'model/',
			'chunksPath' => $corePath . 'elements/chunks/',
			'templatesPath' => $corePath . 'elements/templates/',
			'chunkSuffix' => '.chunk.tpl',
			'snippetsPath' => $corePath . 'elements/snippets/',
			'processorsPath' => $corePath . 'processors/',
			'cookiePath' => $corePath . 'cookies/'
		), $config);

		$this->modx->addPackage('postrequests', $this->config['modelPath']);
		$this->modx->lexicon->load('postrequests:default');
	}

	public function initialize() {
		$this->modx->regClientScript($this->config['jsUrl'].'postrequests.js');
		if (isset($_POST['pr_action'])) {
			$this->requestAction = $_POST['pr_action'];
			unset($_POST['pr_action']);
		}
		if (isset($_POST['url'])) {
			$this->requestUrl = $_POST['url'];
			unset($_POST['url']);
		}

		if (isset($_POST['fieldNames'])) {
			foreach ($_POST['fieldNames'] as $k => $key) {
				$this->request[$key] = $_POST['fieldValues'][$k];
			}
		}
		return true;
	}

	public function execRequest() {
		if ($this->requestAction) {
			switch ($this->requestAction) {
				case 'request':
				case 'auth': exit('<pre>'.$this->fetch().'</pre>'); break;

				default:
					break;

			}
		}

		return;
	}


	public function fetch() {
		$out = '<default output';
		$cookie = $this->config['cookiePath'].'cookie.txt';
		if ($curl = curl_init()) {
			curl_setopt($curl, CURLOPT_URL, $this->requestUrl);
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($curl, CURLOPT_BINARYTRANSFER, true);
			curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
			//curl_setopt($curl, CURLOPT_VERBOSE, true);
			curl_setopt($curl, CURLOPT_USERAGENT, "Mozilla/4.0 (Windows; U; Windows NT 5.0; En; rv:1.8.0.2) Gecko/20070306 Firefox/1.0.0.4");
			curl_setopt($curl, CURLOPT_POST, true);
			curl_setopt($curl, CURLOPT_POSTFIELDS, $this->request);
			curl_setopt($curl, CURLOPT_COOKIEFILE, $cookie);
			curl_setopt($curl, CURLOPT_COOKIEJAR, $cookie);
			$out = curl_exec($curl);
			curl_close($curl);
		}
		return $out."\n\n Input vars: \n\n".print_r($this->request,1);
	}

}