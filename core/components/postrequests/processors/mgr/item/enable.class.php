<?php

/**
 * Enable an Item
 */
class PostRequestsItemEnableProcessor extends modObjectProcessor {
	public $objectType = 'PostRequestsItem';
	public $classKey = 'PostRequestsItem';
	public $languageTopics = array('postrequests');
	//public $permission = 'save';


	/**
	 * @return array|string
	 */
	public function process() {
		if (!$this->checkPermissions()) {
			return $this->failure($this->modx->lexicon('access_denied'));
		}

		$ids = $this->modx->fromJSON($this->getProperty('ids'));
		if (empty($ids)) {
			return $this->failure($this->modx->lexicon('postrequests_item_err_ns'));
		}

		foreach ($ids as $id) {
			/** @var PostRequestsItem $object */
			if (!$object = $this->modx->getObject($this->classKey, $id)) {
				return $this->failure($this->modx->lexicon('postrequests_item_err_nf'));
			}

			$object->set('active', true);
			$object->save();
		}

		return $this->success();
	}

}

return 'PostRequestsItemEnableProcessor';
