<?php
include_once 'setting.inc.php';

$_lang['postrequests'] = 'PostRequests';
$_lang['postrequests_menu_desc'] = 'A sample Extra to develop from.';
$_lang['postrequests_intro_msg'] = 'You can select multiple items by holding Shift or Ctrl button.';

$_lang['postrequests_items'] = 'Items';
$_lang['postrequests_item_id'] = 'Id';
$_lang['postrequests_item_name'] = 'Name';
$_lang['postrequests_item_description'] = 'Description';
$_lang['postrequests_item_active'] = 'Active';

$_lang['postrequests_item_create'] = 'Create Item';
$_lang['postrequests_item_update'] = 'Update Item';
$_lang['postrequests_item_enable'] = 'Enable Item';
$_lang['postrequests_items_enable'] = 'Enable Items';
$_lang['postrequests_item_disable'] = 'Disable Item';
$_lang['postrequests_items_disable'] = 'Disable Items';
$_lang['postrequests_item_remove'] = 'Remove Item';
$_lang['postrequests_items_remove'] = 'Remove Items';
$_lang['postrequests_item_remove_confirm'] = 'Are you sure you want to remove this Item?';
$_lang['postrequests_items_remove_confirm'] = 'Are you sure you want to remove this Items?';

$_lang['postrequests_item_err_name'] = 'You must specify the name of Item.';
$_lang['postrequests_item_err_ae'] = 'An Item already exists with that name.';
$_lang['postrequests_item_err_nf'] = 'Item not found.';
$_lang['postrequests_item_err_ns'] = 'Item not specified.';
$_lang['postrequests_item_err_remove'] = 'An error occurred while trying to remove the Item.';
$_lang['postrequests_item_err_save'] = 'An error occurred while trying to save the Item.';

$_lang['postrequests_grid_search'] = 'Search';
$_lang['postrequests_grid_actions'] = 'Actions';