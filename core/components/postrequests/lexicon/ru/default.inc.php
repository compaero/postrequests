<?php
include_once 'setting.inc.php';

$_lang['postrequests'] = 'PostRequests';
$_lang['postrequests_menu_desc'] = 'Пример расширения для разработки.';
$_lang['postrequests_intro_msg'] = 'Вы можете выделять сразу несколько предметов при помощи Shift или Ctrl.';

$_lang['postrequests_items'] = 'Предметы';
$_lang['postrequests_item_id'] = 'Id';
$_lang['postrequests_item_name'] = 'Название';
$_lang['postrequests_item_description'] = 'Описание';
$_lang['postrequests_item_active'] = 'Активно';

$_lang['postrequests_item_create'] = 'Создать предмет';
$_lang['postrequests_item_update'] = 'Изменить Предмет';
$_lang['postrequests_item_enable'] = 'Включить Предмет';
$_lang['postrequests_items_enable'] = 'Включить Предметы';
$_lang['postrequests_item_disable'] = 'Отключить Предмет';
$_lang['postrequests_items_disable'] = 'Отключить Предметы';
$_lang['postrequests_item_remove'] = 'Удалить Предмет';
$_lang['postrequests_items_remove'] = 'Удалить Предметы';
$_lang['postrequests_item_remove_confirm'] = 'Вы уверены, что хотите удалить этот Предмет?';
$_lang['postrequests_items_remove_confirm'] = 'Вы уверены, что хотите удалить эти Предметы?';
$_lang['postrequests_item_active'] = 'Включено';

$_lang['postrequests_item_err_name'] = 'Вы должны указать имя Предмета.';
$_lang['postrequests_item_err_ae'] = 'Предмет с таким именем уже существует.';
$_lang['postrequests_item_err_nf'] = 'Предмет не найден.';
$_lang['postrequests_item_err_ns'] = 'Предмет не указан.';
$_lang['postrequests_item_err_remove'] = 'Ошибка при удалении Предмета.';
$_lang['postrequests_item_err_save'] = 'Ошибка при сохранении Предмета.';

$_lang['postrequests_grid_search'] = 'Поиск';
$_lang['postrequests_grid_actions'] = 'Действия';