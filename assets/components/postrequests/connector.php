<?php
/** @noinspection PhpIncludeInspection */
require_once dirname(dirname(dirname(dirname(__FILE__)))) . '/config.core.php';
/** @noinspection PhpIncludeInspection */
require_once MODX_CORE_PATH . 'config/' . MODX_CONFIG_KEY . '.inc.php';
/** @noinspection PhpIncludeInspection */
require_once MODX_CONNECTORS_PATH . 'index.php';
/** @var PostRequests $PostRequests */
$PostRequests = $modx->getService('postrequests', 'PostRequests', $modx->getOption('postrequests_core_path', null, $modx->getOption('core_path') . 'components/postrequests/') . 'model/postrequests/');
$modx->lexicon->load('postrequests:default');

// handle request
$corePath = $modx->getOption('postrequests_core_path', null, $modx->getOption('core_path') . 'components/postrequests/');
$path = $modx->getOption('processorsPath', $PostRequests->config, $corePath . 'processors/');
$modx->request->handleRequest(array(
	'processors_path' => $path,
	'location' => '',
));