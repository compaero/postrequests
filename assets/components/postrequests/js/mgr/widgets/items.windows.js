PostRequests.window.CreateItem = function (config) {
	config = config || {};
	if (!config.id) {
		config.id = 'postrequests-item-window-create';
	}
	Ext.applyIf(config, {
		title: _('postrequests_item_create'),
		width: 550,
		autoHeight: true,
		url: PostRequests.config.connector_url,
		action: 'mgr/item/create',
		fields: this.getFields(config),
		keys: [{
			key: Ext.EventObject.ENTER, shift: true, fn: function () {
				this.submit()
			}, scope: this
		}]
	});
	PostRequests.window.CreateItem.superclass.constructor.call(this, config);
};
Ext.extend(PostRequests.window.CreateItem, MODx.Window, {

	getFields: function (config) {
		return [{
			xtype: 'textfield',
			fieldLabel: _('postrequests_item_name'),
			name: 'name',
			id: config.id + '-name',
			anchor: '99%',
			allowBlank: false,
		}, {
			xtype: 'textarea',
			fieldLabel: _('postrequests_item_description'),
			name: 'description',
			id: config.id + '-description',
			height: 150,
			anchor: '99%'
		}, {
			xtype: 'xcheckbox',
			boxLabel: _('postrequests_item_active'),
			name: 'active',
			id: config.id + '-active',
			checked: true,
		}];
	}

});
Ext.reg('postrequests-item-window-create', PostRequests.window.CreateItem);


PostRequests.window.UpdateItem = function (config) {
	config = config || {};
	if (!config.id) {
		config.id = 'postrequests-item-window-update';
	}
	Ext.applyIf(config, {
		title: _('postrequests_item_update'),
		width: 550,
		autoHeight: true,
		url: PostRequests.config.connector_url,
		action: 'mgr/item/update',
		fields: this.getFields(config),
		keys: [{
			key: Ext.EventObject.ENTER, shift: true, fn: function () {
				this.submit()
			}, scope: this
		}]
	});
	PostRequests.window.UpdateItem.superclass.constructor.call(this, config);
};
Ext.extend(PostRequests.window.UpdateItem, MODx.Window, {

	getFields: function (config) {
		return [{
			xtype: 'hidden',
			name: 'id',
			id: config.id + '-id',
		}, {
			xtype: 'textfield',
			fieldLabel: _('postrequests_item_name'),
			name: 'name',
			id: config.id + '-name',
			anchor: '99%',
			allowBlank: false,
		}, {
			xtype: 'textarea',
			fieldLabel: _('postrequests_item_description'),
			name: 'description',
			id: config.id + '-description',
			anchor: '99%',
			height: 150,
		}, {
			xtype: 'xcheckbox',
			boxLabel: _('postrequests_item_active'),
			name: 'active',
			id: config.id + '-active',
		}];
	}

});
Ext.reg('postrequests-item-window-update', PostRequests.window.UpdateItem);