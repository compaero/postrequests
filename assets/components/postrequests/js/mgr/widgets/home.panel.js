PostRequests.panel.Home = function (config) {
	config = config || {};
	Ext.apply(config, {
		baseCls: 'modx-formpanel',
		layout: 'anchor',
		/*
		 stateful: true,
		 stateId: 'postrequests-panel-home',
		 stateEvents: ['tabchange'],
		 getState:function() {return {activeTab:this.items.indexOf(this.getActiveTab())};},
		 */
		hideMode: 'offsets',
		items: [{
			html: '<h2>' + _('postrequests') + '</h2>',
			cls: '',
			style: {margin: '15px 0'}
		}, {
			xtype: 'modx-tabs',
			defaults: {border: false, autoHeight: true},
			border: true,
			hideMode: 'offsets',
			items: [{
				title: _('postrequests_items'),
				layout: 'anchor',
				items: [{
					html: _('postrequests_intro_msg'),
					cls: 'panel-desc',
				}, {
					xtype: 'postrequests-grid-items',
					cls: 'main-wrapper',
				}]
			}]
		}]
	});
	PostRequests.panel.Home.superclass.constructor.call(this, config);
};
Ext.extend(PostRequests.panel.Home, MODx.Panel);
Ext.reg('postrequests-panel-home', PostRequests.panel.Home);
