PostRequests.page.Home = function (config) {
	config = config || {};
	Ext.applyIf(config, {
		components: [{
			xtype: 'postrequests-panel-home', renderTo: 'postrequests-panel-home-div'
		}]
	});
	PostRequests.page.Home.superclass.constructor.call(this, config);
};
Ext.extend(PostRequests.page.Home, MODx.Component);
Ext.reg('postrequests-page-home', PostRequests.page.Home);