var PostRequests = function (config) {
	config = config || {};
	PostRequests.superclass.constructor.call(this, config);
};
Ext.extend(PostRequests, Ext.Component, {
	page: {}, window: {}, grid: {}, tree: {}, panel: {}, combo: {}, config: {}, view: {}, utils: {}
});
Ext.reg('postrequests', PostRequests);

PostRequests = new PostRequests();